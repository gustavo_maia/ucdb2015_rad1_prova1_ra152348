
package cadastroaluno;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Layout extends GridPane {
    GerenciadorAluno ga = new GerenciadorAluno();
    ObservableList<Aluno> alunos = ga.getAlunos();
    
    public Layout(){
        this.setHgap(10);
        this.setVgap(10);
        
        Text texto = new Text();
        texto.setText("Tabela de Alunos");
        texto.setFont(Font.font("Verdana", 20));
        this.add(texto, 1, 0, 2, 1);
        
        final TableView<Aluno> tabela = new TableView<Aluno>();
        tabela.setEditable(true);
        
        Label label = new Label("Clique duplo no atributo para editá-lo");
        this.add(label, 1, 2, 2, 1);
        
        TableColumn nome = new TableColumn("Nome");
        nome.setMinWidth(100);
        nome.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("nome")
        );
        nome.setCellFactory(TextFieldTableCell.forTableColumn());
        nome.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setNome(tabela.getNewValue());
                }
            }
        );
        
        TableColumn ra = new TableColumn("RA");
        ra.setMinWidth(100);
        ra.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("ra")
        );
        ra.setCellFactory(TextFieldTableCell.forTableColumn());
        ra.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setRa(tabela.getNewValue());
                }
            }
        );
        
        TableColumn estado = new TableColumn("Estado");
        estado.setMinWidth(50);
        estado.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("estado")
        );
        estado.setCellFactory(TextFieldTableCell.forTableColumn());
        estado.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setEstado(tabela.getNewValue());
                }
            }
        );
        
        
        TableColumn sexo = new TableColumn("Sexo");
        sexo.setMinWidth(70);
        sexo.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("sexo")
        );
        sexo.setCellFactory(TextFieldTableCell.forTableColumn());
        sexo.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setSexo(tabela.getNewValue());
                }
            }
        );
        
        TableColumn profissao = new TableColumn("Profissão");
        profissao.setMinWidth(100);
        profissao.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("profissao")
        );
        profissao.setCellFactory(TextFieldTableCell.forTableColumn());
        profissao.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setProfissao(tabela.getNewValue());
                }
            }
        );
        
        TableColumn materia = new TableColumn("Matéria");
        materia.setMinWidth(100);
        materia.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("materia")
        );
        materia.setCellFactory(TextFieldTableCell.forTableColumn());
        materia.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setMateria(tabela.getNewValue());
                }
            }
        );
        
        TableColumn nota1 = new TableColumn("Nota 1");
        nota1.setMinWidth(50);
        nota1.setCellValueFactory(
            new PropertyValueFactory<Aluno,Double>("nota1")
        );
        
        
        TableColumn nota2 = new TableColumn("Nota 2");
        nota2.setMinWidth(50);
        nota2.setCellValueFactory(
            new PropertyValueFactory<Aluno,Double>("nota2")
        );
        
        
        TableColumn nota3 = new TableColumn("Nota 3");
        nota3.setMinWidth(50);
        nota3.setCellValueFactory(
            new PropertyValueFactory<Aluno,Double>("nota3")
        );
        
        
        TableColumn nota4 = new TableColumn("Nota 4");
        nota4.setMinWidth(50);
        nota4.setCellValueFactory(
            new PropertyValueFactory<Aluno,Double>("nota4")
        );
        
        TableColumn media = new TableColumn("Média");
        nota4.setMinWidth(50);
        nota4.setCellValueFactory(
            new PropertyValueFactory<Aluno,Double>("media")
        );
       
        
        TableColumn ativo = new TableColumn("Ativo");
        ativo.setMinWidth(50);
        ativo.setCellValueFactory(
            new PropertyValueFactory<Aluno,String>("ativo")
        );
        ativo.setCellFactory(TextFieldTableCell.forTableColumn());
        ativo.setOnEditCommit(
            new EventHandler<CellEditEvent<Aluno, String>>() {
                @Override
                public void handle(CellEditEvent<Aluno, String> tabela) {
                    ((Aluno) tabela.getTableView().getItems().get(
                        tabela.getTablePosition().getRow())
                        ).setAtivo(tabela.getNewValue());
                }
            }
        );
        
        
        TableColumn cb = new TableColumn("Excluir");
        cb.setMinWidth(50);
        cb.setCellValueFactory(
            new PropertyValueFactory<Aluno,CheckBox>("cb")
        );
        
        
        tabela.getColumns().addAll(nome, ra, estado, sexo, profissao, materia, nota1, nota2, nota3,nota4, ativo, cb);
        tabela.setItems(alunos);
        this.add(tabela, 1, 4);
        
        Button excluir = new Button("Excluir");
        excluir.setOnAction(new EventHandler<ActionEvent>() {
            ObservableList<Aluno> aExcl = FXCollections.observableArrayList();;
            @Override
            public void handle(ActionEvent t) {
                for(Aluno a: alunos){
                    if(a.getCb().selectedProperty().getValue() == true){
                        aExcl.add(a);
                    };
                }
                alunos.removeAll(aExcl);
                
                Button b = new Button("OK");
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                    children(new Text("Aluno(s) Excluído(s) com Sucesso"), b).
                    alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close();
                    }
                });
            }
            
        });
        this.add(excluir, 2, 4);
    }
}