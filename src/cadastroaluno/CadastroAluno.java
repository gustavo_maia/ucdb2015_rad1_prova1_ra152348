/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cadastroaluno;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author gustavo_maia
 */
public class CadastroAluno extends Application {
    
    @Override
    public void start(Stage stage) {
        
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);
        
        Text texto = new Text("Cadastro de Alunos");
        texto.setFont(Font.font("Verdana", 20));
        grid.add(texto, 0, 0, 2, 1);
        
        //NOME
        Label nomeL = new Label("Nome: ");
        final TextField nomeF = new TextField();
        grid.add(nomeL, 0, 1);
        grid.add(nomeF, 1, 1);
        
        //RA
        Label raL = new Label("RA: ");
        final TextField raF = new TextField();
        grid.add(raL, 0, 2);
        grid.add(raF, 1, 2);
        
        
        //SEXO
        Label sexoL = new Label("Sexo: ");
        grid.add(sexoL, 0, 3);
        
        HBox caixaSexo = new HBox();
        grid.add(caixaSexo, 1, 3);
        
        final ToggleGroup tg = new ToggleGroup();
        final RadioButton rbMasc = new RadioButton("Masc ");
        rbMasc.setToggleGroup(tg);
        rbMasc.setSelected(true);
        final RadioButton rbFemin = new RadioButton("Femin");
        rbFemin.setToggleGroup(tg);
        
        tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){

            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {
                
            }
            
        });
        caixaSexo.getChildren().addAll(rbMasc, rbFemin);
        
        //ESTADO
        Label estado = new Label("Estado: ");
        grid.add(estado, 0, 4);
        
        final ComboBox cb = new ComboBox();
        cb.getItems().addAll(
                "MS",
                "SP",
                "RJ"
        );
        grid.add(cb, 1, 4);
        
        //PROFISSAO
        Label profissao = new Label("Profissão: ");
        grid.add(profissao, 0, 5);
        
        final ComboBox cbPro = new ComboBox();
        cbPro.getItems().addAll(
                "Estudante",
                "Outro"
        );
        grid.add(cbPro, 1, 5);
        
        //MATERIA
        Label materia = new Label("Matéria: ");
        grid.add(materia, 0, 6);
        
        final ComboBox cbMat = new ComboBox();
        cbMat.getItems().addAll(
                "TAP1",
                "RAD1"
        );
        grid.add(cbMat, 1, 6);
        
        //NOTAS
        Label nota1 = new Label("Nota 1: ");
        final TextField n1F = new TextField();
        grid.add(nota1, 0, 7);
        grid.add(n1F, 1, 7);
        
        Label nota2 = new Label("Nota 2: ");
        final TextField n2F = new TextField();
        grid.add(nota2, 0, 8);
        grid.add(n2F, 1, 8);
        
        Label nota3 = new Label("Nota 3: ");
        final TextField n3F = new TextField();
        grid.add(nota3, 0, 9);
        grid.add(n3F, 1, 9);
        
        Label nota4 = new Label("Nota 4: ");
        final TextField n4F = new TextField();
        grid.add(nota4, 0, 10);
        grid.add(n4F, 1, 10);
        
        //ATIVO
        Label ativo = new Label("Ativo ");
        final CheckBox check = new CheckBox();
        
        HBox HAtivo = new HBox();
        HAtivo.getChildren().addAll(ativo, check);
        grid.add(HAtivo, 0, 11);
        
        //BOTÕES
        HBox caixaBotao = new HBox();
        grid.add(caixaBotao, 1, 12);
        caixaBotao.setAlignment(Pos.BOTTOM_RIGHT);
        caixaBotao.setSpacing(10);
        Button salvar = new Button("Salvar");
        Button limpar = new Button("Limpar");
        Button listar = new Button("Listar");
        caixaBotao.getChildren().addAll(salvar, limpar, listar);
        
        salvar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                Aluno aluno = new Aluno();
                aluno.setNome(nomeF.getText());
                aluno.setRa(raF.getText());
                if(tg.getSelectedToggle() == rbMasc){
                    aluno.setSexo("Masculino");
                }else if(tg.getSelectedToggle() == rbFemin){
                    aluno.setSexo("Feminino");
                }else{
                    aluno.setSexo("Não informado");
                }
                aluno.setEstado(cb.getValue().toString());
                aluno.setProfissao(cbPro.getValue().toString());
                aluno.setMateria(cbMat.getValue().toString());
                aluno.setCb(new CheckBox());
                if(check.selectedProperty().getValue() == true){
                    aluno.setAtivo("SIM");
                }else{
                    aluno.setAtivo("NÃO");
                }
                aluno.setNota1(Double.parseDouble(n1F.getText()));
                aluno.setNota2(Double.parseDouble(n2F.getText()));
                aluno.setNota3(Double.parseDouble(n3F.getText()));
                aluno.setNota4(Double.parseDouble(n4F.getText()));
                double n1, n2, n3, n4;
                n1 = Double.parseDouble(n1F.getText());
                n2 =Double.parseDouble(n2F.getText());
                n3 = Double.parseDouble(n3F.getText());
                n4 =Double.parseDouble(n4F.getText());
                aluno.setMedia((n1+n2+n3+n4)/4);
                
                GerenciadorAluno ga = new GerenciadorAluno();
                ga.salvar(aluno);
                
                Button b = new Button("OK");
                b.setMaxHeight(15);
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                    children(new Text("Aluno Cadastrado com Sucesso"), b).
                    alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close();
                    }
                });
            }
        });
        
        listar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                Stage secondStage = new Stage();
                Layout layout = new Layout();
                
                Scene cena2 = new Scene(layout, 980, 500);
                secondStage.setScene(cena2);
                secondStage.show();
            }
        });
        
        limpar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                raF.setText("");
                nomeF.setText("");
                n1F.setText("");
                n2F.setText("");
                n3F.setText("");
                n4F.setText("");
                rbMasc.setSelected(false);
                rbFemin.setSelected(false);
                cb.setValue(null);
                cbPro.setValue(null);
                cbMat.setValue(null);
                check.selectedProperty().setValue(false);
            }
        });
        
        
        Scene cena = new Scene(grid, 400, 400);
        
        stage.setTitle("Cadastro");
        stage.setScene(cena);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
